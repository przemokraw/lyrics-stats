/**
 * @file Klasa opisująca pojedynczą piosenkę.
 */
package LyricsStats;

import java.util.HashMap;
import java.util.Map;

public class Lyrics {

	private String text;
	private String title;
	
	public Lyrics(String title, String text) {
		this.text = text;
		this.title = title;
	}
	
	public Map<String, Integer> getCountsOfWords() {
		String[] words = text.split("[\\s,.:;?!()-]+");
		Map<String, Integer> result = new HashMap<String, Integer>();
		for (String word: words) {
			if (result.containsKey(word))
				result.put(word, 1 + (result.get(word).intValue()));
			else
				result.put(word, 1);
		}
		return result;
	}
	
	public String getTitle() {
		return title;
	}
	
	@Override
	public String toString() {
		return text;
	}
}
