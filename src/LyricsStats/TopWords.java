package LyricsStats;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TopWords extends Statistics<Map<String, Integer>> {

	private class ValueComparator implements Comparator<String> {
		Map<String, Integer> map;
		public ValueComparator(Map<String, Integer> base) {
			this.map = base;
		}
		public int compare(String a, String b) {
			if (map.get(a) >= map.get(b))
				return -1;
			else
				return 1;
		}
	}
	
	int k; // ile najczęściej używanych słów chcemy?
	
	public TopWords(Collection<String> filters, int k) {
		super(filters);
		this.k = k;
	}
	
	public TopWords(int k) {
		super(null);
		this.k = k;
	}


	@Override
	public Map<String, Integer> count(Artist author) {
		List<Lyrics> texts = author.getAllLyrics();
		Map<String, Integer> result = new HashMap<String, Integer>(); 	
		for (Lyrics text: texts) {
			Map<String, Integer> wordFromOneText = text.getCountsOfWords();
			for (String key: wordFromOneText.keySet()) {
				if (!getFilters().contains(key))
				{
					if (result.containsKey(key))
						result.put(key, result.get(key) + wordFromOneText.get(key));
					else
						result.put(key, wordFromOneText.get(key));
				}
			}
		}
		ValueComparator vc = new ValueComparator(result);
		TreeMap<String, Integer> sortedResult = new TreeMap<String, Integer>(vc);
		sortedResult.putAll(result);
		Map<String, Integer> target = new TreeMap<String, Integer>(vc);
		for (int i = 0; i < this.k; i++) {
			Map.Entry<String, Integer> last = sortedResult.pollFirstEntry();
			target.put(last.getKey(), last.getValue());
		}
		return target;
	}
}
