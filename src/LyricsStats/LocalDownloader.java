package LyricsStats;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class LocalDownloader extends LyricsDownloader {

	private String path;
	
	public LocalDownloader(String path) {
		this.path = path;
	}
	
	@Override
	public List<Lyrics> downloadAll(Artist author) throws IOException {
		// TODO: obsłużyć wyjątek
		String currentFolder = path + "/" + author.toString() + "/";
		File file = new File(currentFolder);
		List<Lyrics> texts = new ArrayList<Lyrics>();
		if (file.listFiles() != null) {
			for(File path: file.listFiles()) {
				if(path.toString().endsWith(".txt"))
				{
					byte bytes[] = Files.readAllBytes(path.toPath());
					String text = new String(bytes, StandardCharsets.UTF_8);
					texts.add(new Lyrics(path.getName(), text));
				}	
			}
		}
		return texts;	
	}

}
