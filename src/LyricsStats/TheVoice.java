package LyricsStats;

import java.io.IOException;

public class TheVoice {

	public static void main(String[] args) throws IOException {
		TheVoiceArgumentParser parser = null;
		try {
			parser = new TheVoiceArgumentParser(new String[]{"source", "source-type", "proccesors", "filters"}, args);

		} catch (Exception e) {
			e.printStackTrace();
		}
		LocalDownloader d = new LocalDownloader(parser.getCommandValue("source"));
		Filter filter;
		if (parser.getCommandValue("filters") != null)
			filter = new Filter(parser.getCommandValue("filters").split("[:]"));
		else
			filter = new Filter(null);
		
		for (String artistName: parser.getUnnamedCommands()) {
			Artist a = new Artist(artistName);
			a.addALlLyrics(d.downloadAll(a));

			System.out.println(a.toString());
			WordCount s = new WordCount(filter.getFilters());
			System.out.println(s.count(a).toString());
			TopWords t = new TopWords(filter.getFilters(), 5);
			System.out.println(t.count(a).toString());
			System.out.println("**********");
		}
	}

}
