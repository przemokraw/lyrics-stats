package LyricsStats;

import java.util.Collection;
import java.util.HashSet;

public abstract class Statistics<T> {
	
	private Collection<String> filters;
	
	public Statistics(Collection<String> filters) {
		if (filters == null)
			this.filters = new HashSet<String>();
		else
			this.filters = filters;
	}
	
	public abstract T count(Artist author);

	public Collection<String> getFilters() {
		return filters;
	}
}
