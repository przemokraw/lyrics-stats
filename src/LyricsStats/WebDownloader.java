package LyricsStats;

import java.util.List;

public abstract class WebDownloader extends LyricsDownloader {

	private String url;
	
	@Override
	public abstract List<Lyrics> downloadAll(Artist author);
}
