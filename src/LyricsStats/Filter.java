package LyricsStats;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

public class Filter {
	
	Set<String> filters = new HashSet<String>();
	
	public Filter(String[] paths) throws IOException {
		if (paths != null)
			for (String filepath: paths)
				filters.addAll(Files.readAllLines(Paths.get(filepath), StandardCharsets.UTF_8));
	}
	
	public Set<String> getFilters() {
		return filters;
	}
}
