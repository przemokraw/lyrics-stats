package LyricsStats;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class WordCount extends Statistics<Integer> {

	public WordCount(Collection<String> filters) {
		super(filters);
	}
	
	public WordCount() {
		super(null);
	}
	
	@Override
	public Integer count(Artist author) {
		List<Lyrics> texts = author.getAllLyrics();
		Map<String, Integer> result = new HashMap<String, Integer>();
		for(Lyrics text: texts)
			result.putAll(text.getCountsOfWords());
		for(Iterator<Map.Entry<String, Integer>> it = result.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, Integer> entry = it.next();
			if (getFilters().contains(entry.getKey()))
				it.remove();
		}
		return new Integer(result.size());
	}
}
