package LyricsStats;

import java.io.IOException;
import java.util.List;

public abstract class LyricsDownloader {
	
	private String name;


	public abstract List<Lyrics> downloadAll(Artist author) throws IOException;

	public String getName() {
		return name;
	}
}
