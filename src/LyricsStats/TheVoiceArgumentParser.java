package LyricsStats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TheVoiceArgumentParser {

	Map<String, String> commandValues = new HashMap<String, String>();
	List<String> allowedCommands = new ArrayList<String>();

	
	List<String> unnamedCommands = new ArrayList<String>();

	
	public TheVoiceArgumentParser(String[] allowedCommands, String[] args) throws Exception {
		
		for (String command: allowedCommands) 
			this.allowedCommands.add(command);
		
		for (String param: args) {
			if (!param.startsWith("--")) {
				unnamedCommands.add(param);
			}
			else {
				for (String allowedCommand: allowedCommands) {
					if (param.startsWith("--" + allowedCommand + "=")) {
						if (commandValues.containsKey(allowedCommand))
							throw new Exception("Powtórzenie polecenia" + allowedCommand);
						else
							commandValues.put(allowedCommand, param.substring(allowedCommand.length()+3)); //+3 dla znaków "--" i '='
					}
				}
			}
		}
	}
	
	public String getCommandValue(String command) {
		if (commandValues.containsKey(command))
			return commandValues.get(command);
		else
			return null;
	}

	public List<String> getUnnamedCommands() {
		return unnamedCommands;
	}
}
