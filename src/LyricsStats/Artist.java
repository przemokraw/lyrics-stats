package LyricsStats;

import java.util.ArrayList;
import java.util.List;

public class Artist {
	
	private String name;
	private List<Lyrics> texts = new ArrayList<Lyrics>();
	
	public Artist(String name) {
		this.name = name;
	}
	
	public List<Lyrics> getAllLyrics() {
		return texts;
	}
	
	public void addLyrics(Lyrics song) {
		texts.add(song);
	}
	
	public void addALlLyrics(List<Lyrics> songs) {
		for(Lyrics song: songs)
			texts.add(song);
	}
	
	@Override
	public String toString() {
		return name;
	}
}
